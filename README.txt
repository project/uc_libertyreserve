﻿/* $Id$ */

-- SUMMARY --

The Uc libertyreserve module provides the Liberty Reserve payment method to stores which are using the Ubercart module

For a full description of the module, visit the project page:
  http://drupal.org/project/uc_libertyreserve

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/uc_libertyreserve


-- REQUIREMENTS --

Ubercart.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Enable the Liberty Reserve payment method and set the Liberty Reserve account and store details in Administer >> Store administration >> Configuration >> Payment settings >> Payment methods (the path is admin/store/settings/payment/edit/methods)


-- CONTACT --

Current maintainer(s):
* Nabil kadimi (kadimi) - http://drupal.org/user/745228
